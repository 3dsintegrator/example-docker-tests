PHONEY: clean update vet test build release down
BUILD_VERSION ?= latest

clean:
	@rm -f dashboard-meta-integration-tests
	@rm -rf vendor/

update:
	@dep ensure -v

vet:
	@go vet -v ./...

test: clean update vet
	@go test -v ./...

run-pipelines: clean update vet